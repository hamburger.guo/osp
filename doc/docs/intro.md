---
sidebar_position: 1
---

# 开发者文档

集成到您的 app**只需简单几个步骤**

### 您需要满足以下环境

- [Node.js](https://nodejs.org/en/download/) 版本 16.14 或者以上。
- 钱包应用，推荐 metamask，如果您使用 chrome，请安装 metamask 的[浏览器扩展](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn)

## 开始集成 sdk

目前我们提供了 js sdk，支持 react 等框架 app，未来还将支持 react native、kotlin、flutter 等框架的客户端 sdk

[JS SDK](/docs/client/js/)
