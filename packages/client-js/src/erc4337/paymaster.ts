import { UserOperationStruct } from "@alchemy/aa-core";
import { Wallet } from "../wallet";
import { PaymasterAndDataRequest } from "../rest_api_generated";
import { hexlify, hexValue } from "ethers/lib/utils";

export interface IPaymaster {
  dummyPaymasterDataMiddleware(
    struct: UserOperationStruct
  ): Promise<UserOperationStruct>;

  paymasterDataMiddleware(
    struct: UserOperationStruct
  ): Promise<UserOperationStruct>;
}

export class OspVerifyPaymaster implements IPaymaster {
  wallet: Wallet;

  constructor(wallet: Wallet) {
    this.wallet = wallet;
  }

  async dummyPaymasterDataMiddleware(
    struct: UserOperationStruct
  ): Promise<UserOperationStruct> {
    const query: {
      sender: string;
      init_code: string;
      call_data: string;
    } = {
      sender: await struct.sender,
      init_code: hexlify(await struct.initCode),
      call_data: (await struct.callData).toString(),
    };
    const subsidy = (
      await this.wallet.request.call(["erc4337", "checkPaymaster"], query)
    ).data?.subsidy;
    console.log("checkParameter>>>>>>>", subsidy);
    if (subsidy) {
      struct.paymasterAndData =
        this.wallet.contractConfig.paymaster_address +
        "0000000000000000000000000000000000000000000000000000000064b3a0e9000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f48ad96b4c694d63b3b4464a0d6daf0878d7980aaf3693a393eee9d61eaaec776e779def14cb8d44ebc351f4a6b5d8ac23bd7aba8a867422b77b8ad067d6adf61b";
    } else {
      struct.paymasterAndData = "0x";
    }
    return struct;
  }

  async paymasterDataMiddleware(
    struct: UserOperationStruct
  ): Promise<UserOperationStruct> {
    const {
      nonce,
      sender,
      initCode,
      callData,
      callGasLimit,
      verificationGasLimit,
      preVerificationGas,
      maxFeePerGas,
      maxPriorityFeePerGas,
    } = struct;
    if (struct.paymasterAndData == "0x") {
      return struct;
    }
    const paymasterParam: PaymasterAndDataRequest = {
      type: "SUBSIDY",
      user_operation: {
        sender: await sender,
        nonce: hexValue(await nonce),
        init_code: hexlify(await initCode),
        call_data: hexlify(await callData),
        call_gas_limit: hexValue(await callGasLimit),
        verification_gas_limit: hexValue(await verificationGasLimit),
        pre_verification_gas: hexValue(await preVerificationGas),
        max_fee_per_gas: hexValue(await maxFeePerGas),
        max_priority_fee_per_gas: hexValue(await maxPriorityFeePerGas),
      },
    };
    console.log("parameterParam>>>>>>>", paymasterParam);
    const parameterAndData = (
      await this.wallet.request.call(
        ["erc4337", "paymasterData"],
        paymasterParam
      )
    ).data.paymaster_and_data;
    console.log("parameterAndData>>>>>>>", parameterAndData);

    struct.paymasterAndData = parameterAndData;
    console.log(struct);
    return {
      ...struct,
      paymasterAndData: parameterAndData,
    };
  }
}
