import { ethers } from "ethers";

import { Client, DefaultGenerics } from "./client";
import { ACCOUNT_TYPE, WALLET_ERROR } from "./constant";
import { FollowModuleParam } from "./rest_api_generated";
import {
  ERC721BurnableUpgradeable__factory,
  OspClient__factory,
} from "typechain-types";
import { Hex, hexToNumber } from "viem";

type ICreateRelationPost = {
  userId: string;
  feedSlug: string;
  data?: {
    followModuleParam: FollowModuleParam;
  };
};

type QueryParamsType = {
  with_reverse: boolean;
  limit: number;
  next_token: string;
  include_target_ids: Array<string>;
  exclude_target_ids: Array<string>;
};

export class Relation<
  StreamFeedGenerics extends DefaultGenerics = DefaultGenerics
> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  /**
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js
   * @method follow
   * @memberof StreamFeed.prototype
   * @param  {string}   targetSlug   Slug of the target feed
   * @param  {string}   targetUserId User identifier of the target feed
   * @param  {object}   [options]      Additional options
   * @param  {number}   [options.limit] Limit the amount of activities copied over on follow
   * @return {Promise<APIResponse>}
   * @example client.follow('user', 'OX001');
   */
  async follow(
    profileId: Hex,
    data: QueryParamsType,
    isOnChain: boolean,
    onCompleted?: () => void
  ) {
    if (isOnChain) {
      return await this.followWithChain(profileId, data, onCompleted);
    } else {
      return await this.followRest(profileId, data);
    }
  }

  async followRest(profileId: string, data: QueryParamsType) {
    const res = await this.client.request.call(
      ["relations", "addProfileFollow"],
      profileId,
      data
    );
    return res;
  }

  async unFollow(profileId: Hex, isOnChain: boolean) {
    if (isOnChain) {
      return await this.unfollowWithChain(profileId);
    } else {
      return await this.unFollowRest(profileId);
    }
  }
  async unFollowRest(profileId: string) {
    const res = await this.client.request.call(
      ["relations", "deleteProfileFollow"],
      profileId
    );
    return res;
  }

  async unfollowWithChain(profileId: Hex, onCompleted?: () => void) {
    let dcId = BigInt(hexToNumber(profileId));
    let followModule = await this.client.wallet.ospClient.getFollowModule(dcId);
    let tokenId = await ERC721BurnableUpgradeable__factory.connect(
      followModule,
      this.client.wallet.etherProvider
    ).tokenOfOwnerByIndex(followModule, 0);
    let unFollowCallData =
      ERC721BurnableUpgradeable__factory.createInterface().encodeFunctionData(
        "burn",
        [tokenId]
      ) as Hex;
    return await this.client.wallet.account.sendOpAndGetResult(
      { data: unFollowCallData },
      onCompleted
    );
  }

  async followWithChain(
    profileId: Hex,
    moduleParamData: QueryParamsType,
    onCompleted?: () => void
  ) {
    let followCallData =
      this.client.wallet.ospClient.interface.encodeFunctionData("follow", [
        [BigInt(hexToNumber(profileId))],
        ["0x"],
      ]) as Hex;
    return await this.client.wallet.account.sendOpAndGetResult(
      { data: followCallData },
      onCompleted
    );
  }

  /**
   * List the user following list
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js#reading-followed-feeds
   * @method following
   * @memberof StreamFeed.prototype
   * @param  {GetFollowOptions}   [options]  Additional options
   * @param  {string[]}   options.filter array of feed id to filter on
   * @param  {number}   options.limit pagination
   * @param  {number}   options.offset pagination
   * @return {Promise<GetFollowAPIResponse>}
   * @example feed.following('user', 'OX11', {limit:10});
   */
  async following(profileId: string, data: QueryParamsType) {
    const res = await this.client.request.call(
      ["relations", "getProfileFollowing"],
      profileId,
      data
    );
    return res;
  }

  /**
   * List the user followers list
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js#reading-feed-followers
   * @method followers
   * @memberof StreamFeed.prototype
   * @param  {GetFollowOptions}   [options]  Additional options
   * @param  {string[]}   options.filter array of feed id to filter on
   * @param  {number}   options.limit pagination
   * @param  {number}   options.offset pagination
   * @return {Promise<GetFollowAPIResponse>}
   * @example feed.followers({limit:10, filter: ['user:1', 'user:2']});
   */
  async followers(profileId: string, data: QueryParamsType) {
    const res = await this.client.request.call(
      ["relations", "listProfileFollowers"],
      profileId,
      data
    );
    return res;
  }
}
