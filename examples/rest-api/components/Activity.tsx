import React from "react";
import { useEffect, useState } from "react";

export const Activity = ({
  address,
  apiClient,
}: {
  address?: string;
  apiClient?: any;
}) => {
  const [activelist, setActivelist] = useState([]);

  const createActivity = async () => {
    await apiClient.activities.addActivity(
      {
        community_id: "0xe",
        content_uri: "ipfs://QmVU8bToAA4UJRaRaxyBWHCTymnr8oQAvR1zfsQ7WVznUb/0",
      },
      {}
    );
  };
  const getActivities = async () => {
    const enrichOption = {
      enrich: true,
      withRecentReactions: true,
      recentReactionsLimit: 20,
      withOwnReactions: true,
      withOwnChildren: true,
      withReactionCounts: true,
      reactionKindsFilter: ["COMMENT"],
      with_profile: false,
      with_community: false,
      with_relations: ["JOIN"],
    };
    const params = {
      a_community: "0xe",
    };
    const obj = {};
    const obj1 = {};
    Object.keys(enrichOption).forEach(
      (val) => (obj[`enrichOption.${val}`] = enrichOption[val])
    );
    Object.keys(params).forEach(
      (val) => (obj1[`params[${val}]`] = params[val])
    );

    const { data, error } = await apiClient.feed.listFeed(
      "aggregated",
      "aggregated_community_home_post",
      {
        ...obj,
        ...obj1,

        limit: 0,
        reverse: true,
        next_token: "",
        exclude_view_ids: [""],
        ranking: "ACTIVITY_TIME",
        mark_read: true,
      }
    );
    if (!error) {
      setActivelist(data.data.rows);
    }
  };

  const getActivityDetail = async (id) => {
    const res = await apiClient.activities.getActivity(id);
    console.log("ppppp", res);
  };
  // 删除暂时不支持
  const deleteActivitiy = async (id) => {
    const res = await apiClient.activities.stopActivity(id);
    console.log("ppppp", res);
  };
  return (
    <div>
      <h2>Activity模块</h2>
      <h3>创建用户的Activity</h3>
      <button onClick={createActivity}>创建用户的Activity</button>
      <br />
      <h3>获取用户所有的Activity</h3>
      <button onClick={getActivities}>获取用户的所有Activity</button>
      <h4>listContent</h4>
      {activelist.map((i) => (
        <div key={i.created}>
          {i.text}
          <button onClick={() => getActivityDetail(i.view_id)}>查看详情</button>
          <button onClick={() => deleteActivitiy(i.view_id)}>删除</button>
        </div>
      ))}
    </div>
  );
};
