import React from "react";
import { useState } from "react";

export const Relation = ({
  address,
  apiClient,
}: {
  address?: string;
  apiClient?: any;
}) => {
  const [followerList, setFollowerList] = useState([]);
  const [followingList, setFollowingList] = useState([]);

  const handleGetFollowingList = async () => {
    const profileId = "0x8";
    const queryBody = {
      with_reverse: true,
      limit: 20,
      next_token: "",
      include_target_ids: [],
      exclude_target_ids: [],
    };

    const { err, data }: any = await apiClient.relations.getProfileFollowing(
      profileId,
      queryBody
    );
    setFollowingList(data?.obj?.rows || []);
    console.log("handleGetFollowingList info", data, err);
  };

  const handleGetFollowerList = async () => {
    const profileId = "0x7";
    const queryBody = {
      with_reverse: true,
      limit: 20,
      next_token: "",
      include_target_ids: [],
      exclude_target_ids: [],
    };

    const { err, data }: any = await apiClient.relations.listProfileFollowers(
      profileId,
      queryBody
    );
    setFollowerList(data?.obj?.rows || []);
    console.log("handleGetFollowerList info", data, err);
  };

  const handleDoFollow = async () => {
    const profileId = "0x7";
    const queryBody = {
      with_reverse: true,
      limit: 20,
      next_token: "",
      include_target_ids: [],
      exclude_target_ids: [],
    };

    const { err, data }: any = await apiClient.relations.addProfileFollow(
      profileId,
      queryBody,
      {}
    );
    if (data?.code === 200) {
      alert("关注成功");
    }
    console.log("handleDoFollow info", data, err);
  };

  const deleteProfileFollow = async () => {
    const profileId = "0x7";

    const { err, data }: any = await apiClient.relations.deleteProfileFollow(
      profileId,
      {}
    );
    if (data?.code === 200) {
      alert("关注成功");
    }
    console.log("handleDoFollow info", data, err);
  };

  const getProfileJoining = async () => {
    const profileId = "0x8";
    const queryBody = {
      with_reverse: true,
      limit: 20,
      next_token: "",
      include_target_ids: [],
      exclude_target_ids: [],
    };

    const { err, data }: any = await apiClient.relations.getProfileJoining(
      profileId,
      queryBody
    );
    if (data?.code === 200) {
    }
    console.log("handleDoFollow info", data, err);
  };

  if (!address) {
    return null;
  }

  return (
    <>
      <h2>Relation 模块</h2>
      <p>
        <h3>获取关注列表</h3>
        <button onClick={handleGetFollowingList}>getFollowingList</button>
        <p>
          {followingList.map((item, index) => {
            return <div key={`${item.owner}:${index}`}>{item.owner}</div>;
          })}
        </p>
      </p>
      <p>
        <h3>获取粉丝列表</h3>
        <button onClick={handleGetFollowerList}>getFollowerList</button>
        <p>
          {followerList.map((item, index) => {
            return <div key={`${item.owner}:${index}`}>{item.owner}</div>;
          })}
        </p>
      </p>
      <p>
        <h3>关注单个用户</h3>
        <button onClick={handleDoFollow}>doFollow</button>
      </p>
      <p>
        <h3>取消关注单个用户</h3>
        <button onClick={deleteProfileFollow}>unDoFollow</button>
      </p>
      <p>
        <h3>获取用户加入的所有community</h3>
        <button onClick={getProfileJoining}>getProfileJoining</button>
      </p>
    </>
  );
};
