import {
  Api,
  CollectModuleEnum,
  ReferenceModuleEnum,
  FollowModuleEnum,
  ReactionKindEnum,
} from "osp-client-js";

export const TypeData = ({
  address,
  apiClient,
}: {
  address?: string;
  apiClient?: Api<{}>;
}) => {
  const profileCreate = async () => {
    await apiClient.typedata.addProfileForTypeData({
      // profile_id/userid
      handle: "abcdesss1",
      follow_module: {
        type: "NULL_FOLLOW_MODULE",
        init: {},
      },
    });
  };
  const communityCreate = async () => {
    await apiClient.typedata.addCommunityForTypeData({
      handle: "asdasdasd",
      display_name: "asdasdasd",
      desc: "asdasdasd",
      logo: "asdasdasd",
      banner: "22",
      template: "1",
      condition: {
        type: "SlotNFTCondition",
        param: {
          SlotNFTCondition: {
            slot_nft_address: "0xc5a5C42992dECbae36851359345FE25997F5C42d",
            token_id: 17,
          },
        },
      },
      join_module: {
        type: "NULL_JOIN_MODULE",
        init: {},
      },
    });
  };

  const joinCreate = async () => {
    await apiClient.typedata.addJoinForTypeData({
      // profile_id/userid
      id: "0x2",
      join_module_param: {
        type: "NULL_JOIN_MODULE",
        data: {
          null_join_module: {},
          fee_join_module: {
            currency_address: "string",
            amount: 0,
          },
        },
      },
    });
  };

  const createActivity = async () => {
    await apiClient.typedata.activityCreate({
      // profile_id/userid
      profile_id: "0xfa",
      content_uri: "ipfs://QmYqrhL8Nzec4swV6bsHsv1ioojJUgRerszRsfeGz8VhhN",
      collect_module: {
        type: CollectModuleEnum.FreeCollectModule,
        init: { FreeCollectModule: { only_follower: false } },
      },
      reference_module: {
        type: ReferenceModuleEnum.FollowerOnlyReferenceModule,
        init: {},
      },
    });
  };

  const followCreate = async () => {
    const res = await apiClient.typedata.addFollowForTypeData({
      id: "0x8",
      follow_module_param: {
        type: "NULL_FOLLOW_MODULE",
        data: {},
      },
    });
    console.log("ppppp", res);
  };

  const reactionCreate = async () => {
    const res = await apiClient.typedata.reactionCreate({
      // profile_id/userid
      kind: ReactionKindEnum.LIKE,
      user_id: "0xfa",
      content_uri: "ipfs://QmYqrhL8Nzec4swV6bsHsv1ioojJUgRerszRsfeGz8VhhN",
      collect_module: {
        type: CollectModuleEnum.FreeCollectModule,
        init: { FreeCollectModule: { only_follower: false } },
      },
      reference_module: { type: ReferenceModuleEnum.NULL, init: {} },
      collect_module_param: { type: CollectModuleEnum.FeeCollectModule },
      reference_module_param: { type: ReferenceModuleEnum.NULL },
      target_activity_id: null,
      // target_user_id:
    });
    console.log("ppppp", res);
  };

  const profileDispatcherCreate = async () => {
    const res = await apiClient.typedata.profileDispatcherCreate({
      dispatcher: "",
      profile_id: "",
    });
    console.log("ppppp", res);
  };
  if (!address) return null;
  return (
    <div>
      <h2>TypeData模块</h2>
      <h3>创建用户的Profile</h3>
      <button onClick={profileCreate}>创建用户的Profile</button>
      <br />
      <h3>创建社区</h3>
      <button onClick={communityCreate}>创建社区</button>
      <br />
      <h3>加入社区</h3>
      <button onClick={joinCreate}>加入社区</button>
      <br />
      <h3>创建用户的Activity</h3>
      <button onClick={createActivity}>创建用户的Activity</button>
      <br />
      <h3>关注用户</h3>
      <button onClick={followCreate}>关注用户</button>
      <br />
      <h3>创建用户的Reaction</h3>
      <button onClick={reactionCreate}>创建用户的Reaction</button>
      <br />
      <h3>创建用户的dispatcher</h3>
      <button onClick={profileDispatcherCreate}>创建用户的dispatcher</button>
    </div>
  );
};
