import React from "react";
import { Client, WALLET_TYPE } from "osp-client-js";

export function Auth({ client }: { client: Client }) {
  const connectWallet = async () => {
    const res = await client.wallet.connect();
    console.log("res", res);
  };

  const logout = async () => {
    await client.auth.signOut();
  };
  const handleSignIn = async (type) => {
    await client.auth.signIn(type);
  };

  const handleSignOut = async () => {
    await client.auth.signOut();
  };
  const getContractAddress = () => {
    console.log(client.wallet.contractConfig);
  };

  const signCommonMessage = async () => {
    let res = await client.wallet.personalSign("hello world");
    alert(JSON.stringify(res));
  };

  const signEip712Message = async () => {
    const originalMessage = {
      types: {
        EIP712Domain: [
          {
            name: "name",
            type: "string",
          },
          {
            name: "version",
            type: "string",
          },
          {
            name: "verifyingContract",
            type: "address",
          },
        ],
        Greeting: [
          {
            name: "contents",
            type: "string",
          },
        ],
      },
      primaryType: "Greeting",
      domain: {
        name: "web3auth",
        version: "1",
        verifyingContract: "0xE0cef4417a772512E6C95cEf366403839b0D6D6D",
      },
      message: {
        contents: "Hello, from web3auth!",
      },
    };
    const res = await client.wallet.signMessage(originalMessage);
    alert(JSON.stringify(res));
  };

  return (
    <>
      <h2>Auth模块</h2>
      <h3>web3Auth</h3>
      <button onClick={connectWallet}>connectWallet</button>
      <button onClick={logout}>logout</button>
      <button
        onClick={() => {
          alert(client.wallet.web3Auth.status);
        }}
      >
        status
      </button>
      <button
        onClick={() => {
          client.wallet.getUserInfo().then((res) => {
            alert(JSON.stringify(res));
            console.log(res);
          });
        }}
      >
        userInfo
      </button>
      <button
        onClick={() => {
          client.wallet.getAccounts().then((res) => {
            alert(JSON.stringify(res));
            console.log(res);
          });
        }}
      >
        accounts
      </button>
      <button
        onClick={() => {
          alert(JSON.stringify(client.wallet.account.address));
          console.log(client.wallet.account);
        }}
      >
        aa-account
      </button>
      <button
        onClick={async () => {
          alert(await client.wallet.getChainId());
        }}
      >
        chian-id
      </button>
      <button onClick={signCommonMessage}>signMessage</button>
      <button onClick={signEip712Message}>sign712</button>

      <h3>登录</h3>

      <button onClick={() => handleSignIn(WALLET_TYPE.METAMASK)}>
        metamask signIn
      </button>
      <button onClick={() => handleSignIn(WALLET_TYPE.GOOGLE)}>
        google signIn
      </button>
      <button onClick={() => handleSignIn(WALLET_TYPE.FACEBOOK)}>
        facebook signIn
      </button>
      <button onClick={() => handleSignIn(WALLET_TYPE.TWITTER)}>
        twitter signIn
      </button>
      <h3>退出</h3>

      <button onClick={handleSignOut}>signOut</button>

      <h3>获取合约地址</h3>

      <button onClick={getContractAddress}>获取合约地址</button>
    </>
  );
}
